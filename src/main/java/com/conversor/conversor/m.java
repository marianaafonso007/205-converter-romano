package com.conversor.conversor;

import java.util.HashMap;
import java.util.Map;

public enum m {
    M(1000),
    MM(2000),
    MMM(3000);

    public int valor;

    public int getValor() {
        return valor;
    }
    m(int valor){
        this.valor = valor;
    }
    private static final Map<Integer, m> mPorValor = new HashMap<>();

    static {
        for(m M : m.values()){
            mPorValor.put(M.getValor(), M);
        }
    }

    public static m consultarDPorValor(int valor){
        return mPorValor.get(valor);
    }
}
