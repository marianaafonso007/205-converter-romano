package com.conversor.conversor;

import java.util.HashMap;
import java.util.Map;

public enum u {
    I(1),
    II(2),
    III(3),
    IV(4),
    V(5),
    VI(6),
    VII(7),
    VIII(8),
    IX(9);

    public int valor;

    public int getValor() {
        return valor;
    }
    u(int valor){
        this.valor = valor;
    }

    private static final Map<Integer, u> uPorValor = new HashMap<>();

    static {
        for(u U : u.values()){
            uPorValor.put(U.getValor(), U);
        }
    }

    public static u consultarDPorValor(int valor){
        return uPorValor.get(valor);
    }

}
