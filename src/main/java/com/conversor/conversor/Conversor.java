package com.conversor.conversor;

import com.sun.xml.internal.ws.commons.xmlutil.Converter;

import java.util.List;

public class Conversor {

    public static String converterNaturalParaDecimal(int numero){
        String convert = Integer.toString(numero);
        int tamanho = convert.length();
        String numeralRomano = "";
        if(tamanho > 3){
            numeralRomano = numeralRomanoMilhar(Integer.parseInt(convert.substring(1,1)));
            convert = convert.substring(2,4);
            tamanho = tamanho -1;
        }
        if (tamanho > 2){
            numeralRomano +=  numeralRomanoCentena(Integer.parseInt(convert.substring(2,2)));
            convert = convert.substring(2,3);
            tamanho = tamanho -1;
        }
        if (tamanho > 1){
            numeralRomano +=  numeralRomanoDezena(Integer.parseInt(convert));
            tamanho = tamanho -1;
            convert = convert.substring(2,2);
        }
        if (tamanho == 1){
            if (Integer.parseInt(convert) != 0){
                numeralRomano +=  numeralRomanoUnidade(Integer.parseInt(convert.substring(2,2)));
            }
        }
        return numeralRomano;
    }

    public static String numeralRomanoMilhar(int numero){
        m milhar = m.consultarDPorValor(numero);
        String retorno = milhar.toString();
        return retorno;
    }

    public static String numeralRomanoCentena(int numero){
        c centena = c.consultarDPorValor(numero);
        String retorno = centena.toString();
        return retorno;
    }

    public static String numeralRomanoDezena(int numero){
        d decimal = d.consultarDPorValor(numero);
        String retorno = decimal.toString();
        return retorno;
    }

    public static String numeralRomanoUnidade(int numero){
        u unidade = u.consultarDPorValor(numero);
        String retorno = unidade.toString();
        return retorno;
    }

}
