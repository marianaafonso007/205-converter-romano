package com.conversor.conversor;

import java.util.HashMap;
import java.util.Map;

public enum c {
    C(100),
    CC(200),
    CCC(300),
    CD(400),
    D(500),
    DC(600),
    DCC(700),
    DCCC(800),
    CM(900);

    public int valor;

    public int getValor() {
        return valor;
    }
    c(int valor){
        this.valor = valor;
    }

    private static final Map<Integer, c> cPorValor = new HashMap<>();

    static {
        for(c C : c.values()){
            cPorValor.put(C.getValor(), C);
        }
    }

    public static c consultarDPorValor(int valor){
        return cPorValor.get(valor);
    }
}
