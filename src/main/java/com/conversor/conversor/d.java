package com.conversor.conversor;

import java.util.HashMap;
import java.util.Map;

public enum d {
    X(10),
    XX(20),
    XXX(30),
    XL(40),
    L(50),
    LX(60),
    LXX(70),
    LXXX(80),
    XC(90);

    public int valor;

    public int getValor() {
        return valor;
    }
    d(int valor){
        this.valor = valor;
    }

    private static final Map<Integer, d> dPorValor = new HashMap<>();

    static {
        for(d D : d.values()){
            dPorValor.put(D.getValor(), D);
        }
    }

    public static d consultarDPorValor(int valor){
        return dPorValor.get(valor);
    }
}
