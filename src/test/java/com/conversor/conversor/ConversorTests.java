package com.conversor.conversor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTests {
    //@Test
    //public void testarAdicionarLanceSucesso(){
      //  int numero = 10; 
        //Conversor.converterNaturalParaDecimal(numero);
        //Assertions.assertEquals(Conversor.converterNaturalParaDecimal(numero), "X");
    //}
    @Test
    public void testarNumeraRomanoDezenaSucesso(){
        int numero = 10;
        Assertions.assertEquals(Conversor.numeralRomanoDezena(numero), "X");
    }
    @Test
    public void testarNumeraRomanoCentenaSucesso(){
        int numero = 100;
        Assertions.assertEquals(Conversor.numeralRomanoCentena(numero), "C");
    }
    @Test
    public void testarNumeraRomanoMilharSucesso(){
        int numero = 2000;
        Assertions.assertEquals(Conversor.numeralRomanoMilhar(numero), "MM");
    }
    @Test
    public void testarNumeraRomanoUnidadeSucesso(){
        int numero = 2;
        Assertions.assertEquals(Conversor.numeralRomanoUnidade(numero), "II");
    }

}
